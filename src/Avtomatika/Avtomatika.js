import React, { useState } from "react";
import { Col, Pagination, Row, Spin } from "antd";
import { useActomatika } from "../Querys/query";
import AvtomatikaCard from "./AvtomatikaCard";
import "./Avtomatika.scss";
import { Link } from "react-router-dom";
import NewElektrotexnika from "../Home/NewElektrotexnika";
import "./Responsive.scss";
import { Helmet } from "react-helmet-async";

function Avtomatika() {
  const [totalPage, setTotalPage] = useState(1);
  const { data, isLoading } = useActomatika(totalPage);
  const clickLink = () => {
    window.scrollTo({
      top: 80
    });
  };
  return (
    <>
      <Helmet>
        <title>Elcom - Avtomatika</title>
        <meta
          name="description"
          content="Avtomatika, Avtomatikaga doir qiziqarli maqolalar, Avtomatika qurilmalari, Avtomatika adabiyotlar"
        />
        <link rel="canonical" href="/avtomatika" />
        <meta name="robots" content="index,follow" />
        <meta
          name="keywords"
          content="Elcom Avtomatika, Avtomatika Avtomatika maqola, Avtomatika kitob, Avtomatika savollar"
        />
        <meta
          property="og:title"
          content="Avtomatika Avtomatika maqola, Avtomatika kitob, Avtomatika savollar"
        />
        <meta
          property="og:description"
          content="Avtomatika, Avtomatikaga doir qiziqarli maqolalar, Avtomatika qurilmalari, Avtomatika adabiyotlar, Elcom Avtomatika"
        />
        <meta property="og:type" content="website" />
        <meta property="og:url" content={`https://elcominfo.uz/avtomatika`} />
        <meta property="og:site_name" content="elcominfo.uz" />
        <meta
          property="og:image"
          content="https://admin.elcominfo.uz/wp-content/uploads/2022/04/facicon-1.png"
        />
        <meta
          type="image/png"
          name="link"
          href="https://admin.elcominfo.uz/wp-content/uploads/2022/04/facicon-1.png"
        />
        <meta property="og:locale" content="uz_uz" />

        {/* Twitter Card SEO  */}

        <meta
          name="twitter:title"
          content="Avtomatika Avtomatika maqola, Avtomatika kitob, Avtomatika savollar."
        />
        <meta
          name="twitter:description"
          content="Avtomatika, Avtomatika doir qiziqarli maqolalar, Avtomatika qurilmalari, Avtomatika adabiyotlar."
        />
        <meta
          name="twitter:image"
          content=" https://admin.elcominfo.uz/wp-content/uploads/2022/04/facicon-1.png"
        />
        <meta name="twitter:card" content="summary_large_image" />
      </Helmet>
      <section className="avtomatika">
        <div className="container-fluid">
          <Row>
            <Col md={{ span: 14 }} style={{ paddingRight: "30px" }}>
              <h3 className="title-large">Avtomatika</h3>
              <Spin spinning={isLoading}>
                {data?.data?.map((data) => {
                  return (
                    <Link to={`/posts/${data.id}`} onClick={clickLink}>
                      <AvtomatikaCard
                        title={data.title.rendered.slice(0, 100).concat("...")}
                        text={data.content.rendered.slice(0, 320).concat("...")}
                        img={data?._embedded["wp:featuredmedia"]?.map((img) => {
                          return img.source_url;
                        })}
                        date={data.date
                          ?.replace(/-/g, ".")
                          .slice(0, 10)
                          .split(".")
                          .reverse()
                          .join(".")}
                        // author={data?._embedded["wp:featuredmedia"]?.map(
                        //   (author) => {
                        //     return author.caption.rendered;
                        //   }
                        // )}
                      />
                    </Link>
                  );
                })}
              </Spin>
              <div className="paginations">
                {data?.headers["x-wp-total"] > 10 ? (
                  <Pagination
                    defaultCurrent={1}
                    total={Number(data?.headers["x-wp-total"])}
                    onChange={(page) => setTotalPage(page)}
                  />
                ) : null}
              </div>
            </Col>
            <Col md={{ span: 10 }}>
              <NewElektrotexnika />
            </Col>
          </Row>
        </div>
      </section>
    </>
  );
}

export default Avtomatika;
