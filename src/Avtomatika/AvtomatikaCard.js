import React from "react";
// import eye from "./Eye.png";
import DateIcon from "../Elektrotexnika/DateIcon";

function AvtomatikaCard(props) {
  return (
    <div className="avtomatikaCard">
      <div className="avtomatika-card_img">
        <div>
          <img src={props.img} alt="" />
        </div>
      </div>
      <div className="avtomatika-card_info">
        <h5 dangerouslySetInnerHTML={{ __html: props.title }}></h5>
        <div className="avtomatika-card_date">
          <p>
            <span>
              {/* <img src={eye} /> */}
              <DateIcon />
              {props.date}
            </span>
            <span dangerouslySetInnerHTML={{ __html: props.author }}></span>
          </p>
        </div>
        <p
          className="avtomatika-card_text"
          dangerouslySetInnerHTML={{ __html: props.text }}
        ></p>
      </div>
      <div></div>
    </div>
  );
}

export default AvtomatikaCard;
