import { Col, Pagination, Row, Spin, Tabs } from "antd";
import React, { useState } from "react";
// import { Link, Route, Routes } from "react-router-dom";
import {
  useAvtomatikaBooks,
  useAvtomatikaBooksMedia,
  useElektronikaBooksMedia,
  useElektrotexnikaBooks,
  useElektrotexnikaBooksMedia,
  useElektronikaBooks,
  useMedia
} from "../Querys/query";
import NewElektronika from "../Elektrotexnika/NewElektronika";
// import { useAdabiyotlar } from "../Querys/query";
// import BooksCard from "./BooksCard";
import "./Books.scss";
import "./Responsive.scss";
import BooksCard from "./BooksCard";
import { Helmet } from "react-helmet-async";
const { TabPane } = Tabs;
// import { Helmet } from "react-helmet";

function Adabiyotlar() {
  const [totalPage, setTotalPage] = useState(1);
  // const { data: BooksData, isLoading: BooksLoading } = useAdabiyotlar();
  const { data: ElektrotexnikaBooks, isLoading: ElektrotexnikaLoading } =
    useElektrotexnikaBooks(totalPage);
  const { data: ElektrotexnikaBooksMedia } =
    useElektrotexnikaBooksMedia(totalPage);
  const { data: ElektronikaBooks, isLoading: ElektronikaBooksLoading } =
    useElektronikaBooks(totalPage);
  const { data: ElektronikaBooksMedia } = useElektronikaBooksMedia(totalPage);
  const { data: AvtomatikaBooks, isLoading: AvtomatikaBooksLoading } =
    useAvtomatikaBooks(totalPage);
  const { data: AvtomatikaBooksMedia } = useAvtomatikaBooksMedia(totalPage);
  const callback = (key) => {
    console.log(key);
  };
  // console.log(ElektrotexnikaBooks);
  // let hrefArr =
  //   ElektrotexnikaBooks &&
  //   ElektrotexnikaBooks?.data?.content?.rendered.split("=");
  // console.log(
  //   ElektrotexnikaBooks &&
  //     ElektrotexnikaBooks?.data?.content?.rendered.split("=")
  // );
  const { data: Media } = useMedia();
  // console.log(Media);
  // const cutText = "(<object(.+?)<\\/object>)";
  const cutLink = '(data=\\"(.+?"))';
  console.log(
    ElektrotexnikaBooks?.data?.map((post) =>
      post.content?.rendered.match(cutLink)
    )
  );
  return (
    <>
      <Helmet>
        <title>Elcom - Adabiyotlar,kitoblar</title>
        <meta
          name="description"
          content="Elcom Adabiyotlar, Elektronika Adabiyotlar, Avtomatika Adabiyotlar, Elektrotexnika Adabiyotlar to'plami. Siz izlagan kitoblar shu yerda"
        />
        <link rel="canonical" href="/books" />
        <meta name="robots" content="index,follow" />
        <meta
          name="keywords"
          content="Elcom Adabiyotlar, Elektronika Adabiyotlar, Avtomatika Adabiyotlar, Elektrotexnika Adabiyotlar"
        />
        <meta
          property="og:title"
          content="Elcom Adabiyotlar, Elektronika Adabiyotlar, Avtomatika Adabiyotlar, Elektrotexnika Adabiyotlar"
        />
        <meta
          property="og:description"
          content="Elcom Adabiyotlar, Elektronika Adabiyotlar, Avtomatika Adabiyotlar, Elektrotexnika Adabiyotlar to'plami. Siz izlagan kitoblar shu yerda"
        />
        <meta property="og:type" content="website" />
        <meta property="og:url" content={`https://elcominfo.uz/books`} />
        <meta property="og:site_name" content="elcominfo.uz" />
        <meta
          property="og:image"
          content="https://admin.elcominfo.uz/wp-content/uploads/2022/04/facicon-1.png"
        />
        <meta
          type="image/png"
          name="link"
          href="https://admin.elcominfo.uz/wp-content/uploads/2022/04/facicon-1.png"
        />
        <meta property="og:locale" content="uz_uz" />

        {/* Twitter Card SEO  */}

        <meta
          name="twitter:title"
          content="Elcom Adabiyotlar, Elektronika Adabiyotlar, Avtomatika Adabiyotlar, Elektrotexnika Adabiyotlar"
        />
        <meta
          name="twitter:description"
          content="Elcom Adabiyotlar, Elektronika Adabiyotlar, Avtomatika Adabiyotlar, Elektrotexnika Adabiyotlar to'plami. Siz izlagan kitoblar shu yerda."
        />
        <meta
          name="twitter:image"
          content=" https://admin.elcominfo.uz/wp-content/uploads/2022/04/facicon-1.png"
        />
        <meta name="twitter:card" content="summary_large_image" />
      </Helmet>
      <section className="books">
        <div className="container-fluid">
          <Row>
            <Col md={{ span: 12 }}>
              <h3 className="title-large">Adabiyotlar</h3>
              <Tabs defaultActiveKey="1" onChange={callback}>
                <TabPane tab="Elektrotexnika" key="1">
                  <Spin spinning={ElektrotexnikaLoading}>
                    {ElektrotexnikaBooks?.data?.map((post) =>
                      Media?.data?.map((media) =>
                        media?.post === post?.id ? (
                          <BooksCard
                            title={post?.title?.rendered}
                            date={post?.date
                              ?.replace(/-/g, ".")
                              .slice(0, 10)
                              .split(".")
                              .reverse()
                              .join(".")}
                            href={media?.source_url}
                          />
                        ) : null
                      )
                    )}
                  </Spin>
                  <div className="bookPagination">
                    {ElektrotexnikaBooks?.headers["x-wp-total"] > 10 ? (
                      <Pagination
                        defaultCurrent={1}
                        total={Number(
                          ElektrotexnikaBooks?.headers["x-wp-total"]
                        )}
                        onChange={(page) => setTotalPage(page)}
                      />
                    ) : null}
                  </div>
                </TabPane>
                <TabPane tab="Elektronika" key="2">
                  <Spin spinning={ElektronikaBooksLoading}>
                    {ElektronikaBooks?.data?.map(
                      (post) =>
                        Media?.data?.map((media) =>
                          media?.post === post?.id ? (
                            <BooksCard
                              title={post?.title?.rendered}
                              date={post?.date
                                ?.replace(/-/g, ".")
                                .slice(0, 10)
                                .split(".")
                                .reverse()
                                .join(".")}
                              href={media?.source_url}
                            />
                          ) : null
                        )
                      // <BooksCard title={post?.title.rendered} />
                    )}
                  </Spin>
                  <div className="bookPagination">
                    {ElektrotexnikaBooks?.headers["x-wp-total"] > 10 ? (
                      <Pagination
                        defaultCurrent={1}
                        total={Number(
                          ElektrotexnikaBooks?.headers["x-wp-total"]
                        )}
                        onChange={(page) => setTotalPage(page)}
                      />
                    ) : null}
                  </div>
                </TabPane>
                <TabPane tab="Avtomatika" key="3">
                  <Spin spinning={AvtomatikaBooksLoading}>
                    {AvtomatikaBooks?.data?.map((post) =>
                      Media?.data?.map((media) =>
                        media?.post === post?.id ? (
                          <BooksCard
                            title={post?.title?.rendered}
                            date={post?.date
                              ?.replace(/-/g, ".")
                              .slice(0, 10)
                              .split(".")
                              .reverse()
                              .join(".")}
                            href={media?.source_url}
                          />
                        ) : null
                      )
                    )}
                  </Spin>
                  <div className="bookPagination">
                    {ElektrotexnikaBooks?.headers["x-wp-total"] > 10 ? (
                      <Pagination
                        defaultCurrent={1}
                        total={Number(
                          ElektrotexnikaBooks?.headers["x-wp-total"]
                        )}
                        onChange={(page) => setTotalPage(page)}
                      />
                    ) : null}
                  </div>
                </TabPane>
              </Tabs>
            </Col>
            <Col md={{ span: 12 }}>
              <NewElektronika />
            </Col>
          </Row>
        </div>
      </section>
    </>
  );
}

export default Adabiyotlar;

{/* <Pagination defaultCurrent={1} total={Number(ElektrotexnikaBooks?.headers["x-wp-total"])} onChange={(page) => setTotalPage(page)}/> */}
