import React from "react";
import BooksImg from "./folder.png";
import Download from "./download.png";

function BooksCard(props) {
  return (
    <div className="booksCard">
      <div className="booksText">
        <div className="books_img">
          <img src={BooksImg} alt="" />
          <img src="" alt="" />
        </div>
        <div className="books_info">
          <div className="booksTitle">
            <h4 dangerouslySetInnerHTML={{ __html: props.title }}></h4>
          </div>
          <div className="booksFormat">
            <p>{props.date}</p>
          </div>
        </div>
      </div>
      <a className="booksDownload" href={props.href}>
        <img src={Download} alt="" />
        {/* <p dangerouslySetInnerHTML={{ __html: props.href }}></p> */}
      </a>
    </div>
  );
}

export default BooksCard;
