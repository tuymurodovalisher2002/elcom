import React from "react";
import { Col, Row, Spin } from "antd";
import { useNews } from "../Querys/query";
import "./News.scss";
import quotes from "./quotes.png";
import eye from "./Eye.png";

function NewsSecond() {
  const { data: newsData, isLoading: newsLoading } = useNews();
  return (
    <section className="news">
      <div className="container-fluid">
        <Row>
          <Col span={14}>
            <Spin spinning={newsLoading}>
              <div className="news-info">
                <h3
                  dangerouslySetInnerHTML={{
                    __html: newsData && newsData[2].title.rendered,
                  }}
                ></h3>
                <div>
                  <h5
                    dangerouslySetInnerHTML={{
                      __html: newsData && newsData[2].excerpt.rendered,
                    }}
                  ></h5>
                  <p>
                    <img src={eye} />
                    <span
                      dangerouslySetInnerHTML={{
                        __html: newsData && newsData[2].id,
                      }}
                    >
                      {/* <img src={eye} /> */}
                    </span>
                    <span
                      dangerouslySetInnerHTML={{
                        __html: newsData && newsData[2].date,
                      }}
                    ></span>
                  </p>
                </div>
                <img
                  src={
                    newsData &&
                    newsData[2]?._embedded["wp:featuredmedia"]?.map((img) => {
                      return img.media_details.sizes.full.source_url;
                    })
                  }
                />
                <div className="quites">
                  <img src={quotes} />
                </div>
                <p
                  dangerouslySetInnerHTML={{
                    __html: newsData && newsData[2].content.rendered,
                  }}
                  className="news-text"
                ></p>
              </div>
            </Spin>
          </Col>
          <Col span={10}></Col>
        </Row>
      </div>
    </section>
  );
}

export default NewsSecond;
