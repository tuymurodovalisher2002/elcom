import { Col, Row, Spin } from "antd";
import { useParams } from "react-router-dom";
import React from "react";
import { usePost } from "../Querys/query";
import "./News.scss";
import quotes from "./quotes.png";
// import eye from "./Eye.png";
// import NewElektronika from "../Elektrotexnika/NewElektronika";
import NewsArticle from "../Home/NewsArticle";
// import telegram from "./telegram.png";
// import youtube from "./youtube.png";
import "./Responsive.scss";
import { Helmet } from "react-helmet-async";
import {
  FacebookIcon,
  FacebookShareButton,
  TelegramIcon,
  TelegramShareButton,
  TwitterIcon,
  TwitterShareButton
} from "react-share";

function News1() {
  const params = useParams();
  const { data: newsData, isLoading: newsLoading } = usePost(params.id);

  return (
    <>
      <Helmet>
        <title>
          {newsData?.title.rendered
            .replace(/<\/?[^>]+>/gi, "")
            .replace(/&#8217;/gi, "'")
            .slice(0, 50)}
        </title>
        <link rel="canonical" href={"/news/" + newsData?.id} />
        <meta name="robots" content="noindex" />
        <meta
          name="description"
          content={newsData?.content.rendered
            .replace(/<\/?[^>]+>/gi, "")
            .replace(/&#8217;/gi, "'")
            .slice(0, 110)
            .concat("...")}
        />
        <meta name="keywords" content={newsData?.title.rendered} />
        <meta
          property="og:title"
          content={newsData?.title.rendered
            .replace(/<\/?[^>]+>/gi, "")
            .replace(/&#8217;/gi, "'")
            .slice(0, 50)
            .concat("...")}
        />
        <meta
          property="og:description"
          content={newsData?.content.rendered
            .replace(/<\/?[^>]+>/gi, "")
            .replace(/&#8217;/gi, "'")
            .slice(0, 120)
            .concat("...")}
        />
        <meta property="og:type" content="website" />
        <meta
          property="og:url"
          content={`https://elcominfo.uz/news/` + newsData?.id}
        />
        <meta property="og:site_name" content="elcominfo.uz" />
        <meta
          property="og:image"
          content={newsData?._embedded["wp:featuredmedia"]?.map(
            (img) => img.media_details.sizes.full.source_url
          )}
        />
        <meta property="og:image:witdh" content="1200" />
        <meta property="og:image:height" content="630" />
        <meta property="og:image:alt" content="Yangi maqola" />
        <meta property="og:locale" content="uz_uz" />

        {/* Twitter Card SEO  */}

        <meta
          name="twitter:title"
          content={newsData?.title.rendered
            .replace(/<\/?[^>]+>/gi, "")
            .replace(/&#8217;/gi, "'")
            .slice(0, 50)}
        />
        <meta
          name="twitter:description"
          content={newsData?.content.rendered
            .replace(/<\/?[^>]+>/gi, "")
            .replace(/&#8217;/gi, "'")
            .slice(0, 110)
            .concat("...")}
        />
        <meta
          name="twitter:image"
          content={
            newsData?._embedded["wp:featuredmedia"]
              ? newsData?._embedded["wp:featuredmedia"]?.map((img) => {
                  return img.source_url;
                })
              : "https://admin.elcominfo.uz/wp-content/uploads/2022/04/facicon-1.png"
          }
        />
        <meta name="twitter:card" content="summary_large_image" />
      </Helmet>
      <section className="news">
        <div className="container-fluid">
          <Row>
            <Col md={{ span: 13 }}>
              <Spin spinning={newsLoading}>
                <div className="news-info">
                  <h3
                    dangerouslySetInnerHTML={{
                      __html: newsData?.title.rendered
                    }}
                  ></h3>
                  <div>
                    <h5
                      dangerouslySetInnerHTML={{
                        __html: newsData?.excerpt.rendered
                      }}
                    ></h5>
                    <p>
                      {/* <img src={eye} alt="" />
                    <span
                      dangerouslySetInnerHTML={{
                        __html: newsData?.,
                      }}
                    >
                    </span> */}
                      <span
                        dangerouslySetInnerHTML={{
                          __html: newsData?.date
                            ?.replace(/-/g, ".")
                            .slice(0, 10)
                            .split(".")
                            .reverse()
                            .join(".")
                        }}
                      ></span>
                    </p>
                  </div>
                  <img
                    alt=""
                    src={newsData?._embedded["wp:featuredmedia"]?.map(
                      (img) => img.media_details.sizes.full.source_url
                    )}
                  />
                  <div className="quites">
                    <img src={quotes} alt="" />
                  </div>
                  <p
                    dangerouslySetInnerHTML={{
                      __html: newsData?.content.rendered
                    }}
                    className="news-text"
                  ></p>
                </div>
                <div className="newsSocialNetwork">
                  <p>Elcom</p>
                  <div>
                    Ulashish
                    {/* <a href="https://t.me/ELCOM_ELEKTRONIKA">
                    <img src={telegram} alt="" />
                  </a>
                  <a href="https://www.youtube.com/channel/UCb1F5CyKZ2R_ppfC8zwvHsQ">
                    <img src={youtube} alt="" />
                  </a> */}
                    <FacebookShareButton
                      style={{ marginLeft: "11px" }}
                      url={"http://elcominfo.uz/" + `news/${newsData?.id}`}
                      quote="Elcominfo. Elektronika, Avtomatika va Elektrotexnikaga doir barcha kerakli manbalar to'plami"
                    >
                      <FacebookIcon size={24} round={true} />
                    </FacebookShareButton>
                    <TelegramShareButton
                      style={{ marginLeft: "11px" }}
                      url={"http://elcominfo.uz/" + `news/${newsData?.id}`}
                      quote="Elcominfo. Elektronika, Avtomatika va Elektrotexnikaga doir barcha kerakli manbalar to'plami"
                    >
                      <TelegramIcon size={24} round={true} />
                    </TelegramShareButton>
                    <TwitterShareButton
                      style={{ marginLeft: "11px" }}
                      url={"http://elcominfo.uz/" + `news/${newsData?.id}`}
                      quote="Elcominfo. Elektronika, Avtomatika va Elektrotexnikaga doir barcha kerakli manbalar to'plami"
                    >
                      <TwitterIcon size={24} round={true} />
                    </TwitterShareButton>
                  </div>
                </div>
              </Spin>
            </Col>
            <Col md={{ span: 11 }}>
              <NewsArticle />
            </Col>
          </Row>
        </div>
      </section>
    </>
  );
}

export default News1;
