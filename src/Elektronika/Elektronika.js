import { Col, Pagination, Row, Spin } from "antd";
import React, { useState } from "react";
import { useElektronika } from "../Querys/query";
import ElektronikaCard from "./ElektronikaCard";
import "./Elektronika.scss";
import { Link } from "react-router-dom";
import NewAvtomatika from "./NewAvtomatika";
import "./Responsive.scss";
import { Helmet } from "react-helmet-async";

function Elektronika() {
  const [totalPage, setTotalPage] = useState(1);
  const { data, isLoading } = useElektronika(totalPage);
  const clickLink = () => {
    window.scrollTo({
      top: 80
    });
  };
  return (
    <>
      <Helmet>
        <title>Elcom - Elektronika</title>
        <meta
          name="description"
          content="Elektronika, Elektronika doir qiziqarli maqolalar, Elektronika qurilmalari, Elektronika adabiyotlar"
        />
        <link rel="canonical" href="/elektronika" />
        <meta name="robots" content="index,follow" />
        <meta
          name="keywords"
          content="Elcom Elektronika, Elektronika, Elektronika maqola, Elektronika kitob, Elektronika savollar"
        />
        <meta
          property="og:title"
          content="Elektronika Elektronika maqola, Elektronika kitob, Elektronika savollar"
        />
        <meta
          property="og:description"
          content="Elektronika, Elektronika doir qiziqarli maqolalar, Elektronika qurilmalari, Elektronika adabiyotlar, Elcom Elektronika"
        />
        <meta property="og:type" content="website" />
        <meta
          property="og:url"
          content={`https://elcominfo.uz/elektrotexnika`}
        />
        <meta property="og:site_name" content="elcominfo.uz" />
        <meta
          property="og:image"
          content="https://admin.elcominfo.uz/wp-content/uploads/2022/04/facicon-1.png"
        />
        <meta
          type="image/png"
          name="link"
          href="https://admin.elcominfo.uz/wp-content/uploads/2022/04/facicon-1.png"
        />
        <meta property="og:locale" content="uz_uz" />

        {/* Twitter Card SEO  */}

        <meta
          name="twitter:title"
          content="Elektronika Elektronika maqola, Elektronika kitob, Elektronika savollar."
        />
        <meta
          name="twitter:description"
          content="Elektronika, Elektronika doir qiziqarli maqolalar, Elektronika qurilmalari, Elektronika adabiyotlar."
        />
        <meta
          name="twitter:image"
          content=" https://admin.elcominfo.uz/wp-content/uploads/2022/04/facicon-1.png"
        />
        <meta name="twitter:card" content="summary_large_image" />
      </Helmet>
      <section className="elektronika">
        <div className="container-fluid">
          <Row>
            <Col md={{ span: 14 }} style={{ paddingRight: "30px" }}>
              <h3 className="title-large">Elektronika</h3>
              <Spin spinning={isLoading}>
                {data?.data?.map((data) => {
                  return (
                    <Link to={`/posts/${data.id}`} onClick={clickLink}>
                      <ElektronikaCard
                        title={data.title.rendered.slice(0, 40).concat("...")}
                        text={data.content.rendered.slice(0, 380).concat("...")}
                        img={data?._embedded["wp:featuredmedia"]?.map((img) => {
                          return img.source_url;
                        })}
                        date={data.date
                          ?.replace(/-/g, ".")
                          .slice(0, 10)
                          .split(".")
                          .reverse()
                          .join(".")}
                        // author={data?._embedded["wp:featuredmedia"]?.map(
                        //   (author) => {
                        //     return author.caption.rendered;
                        //   }
                        // )}
                      />
                    </Link>
                  );
                })}
              </Spin>
              <div className="paginations">
                {data?.headers["x-wp-total"] > 10 ? (
                  <Pagination
                    defaultCurrent={1}
                    total={Number(data?.headers["x-wp-total"])}
                    onChange={(page) => setTotalPage(page)}
                  />
                ) : null}
              </div>
            </Col>
            <Col md={{ span: 10 }}>
              <NewAvtomatika />
            </Col>
          </Row>
        </div>
      </section>
    </>
  );
}

export default Elektronika;
