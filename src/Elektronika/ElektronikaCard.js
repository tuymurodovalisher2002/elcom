import React from "react";
// import eye from "./Eye.png";
import DateIcon from "../Elektrotexnika/DateIcon";

function ElektronikaCard(props) {
  return (
    <div className="electronikaCard">
      <div className="electronika-card_img">
        <div>
          <img src={props.img} alt="" />
        </div>
      </div>
      <div className="electronika-card_info">
        <h5 dangerouslySetInnerHTML={{ __html: props.title }}></h5>
        <div className="electronika-card_date">
          <p>
            <span>
              {/* <img src={eye} alt="" /> */}
              <DateIcon />
              {props.date}
            </span>
            <span dangerouslySetInnerHTML={{ __html: props.author }}></span>
          </p>
        </div>
        <p
          className="electronika-card_text"
          dangerouslySetInnerHTML={{ __html: props.text }}
        ></p>
      </div>
      <div></div>
    </div>
  );
}

export default ElektronikaCard;
