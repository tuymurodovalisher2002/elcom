import React, { useEffect, useState } from "react";
import { Link, useSearchParams, useNavigate } from "react-router-dom";
// import { useSearch } from "../Querys/query";
import "./Header.scss";
import Brand from "./Brand.png";
import "../../../src/Antd.scss";
// import { Select, Input } from "antd";
import "../Responsive.scss";
import Menu from "./Menu";
import Logo from "./LOGO.png";
import { Drawer, Input } from "antd";
import { useSearch } from "../../Querys/query";
// const { Search } = Input;
// const { Option } = Select;
const { Search } = Input;

function Header() {
  const [visible, setVisible] = useState(false);

  const showDrawer = () => {
    setVisible(true);
  };

  const onClose = () => {
    setVisible(false);
  };
  const push = useNavigate();
  // const [menu, setMenu] = useState(false);
  // const [navCount, setNavCount] = useState(false);
  // const clickMenu = () => {
  //   setNavCount(!navCount);
  //   if (navCount) {
  //     setMenu(true);
  //   } else {
  //     setMenu(false);
  //   }
  // };
  // const clickLink = () => {
  //   setMenu(false);
  // };
  // const [totalPage, setTotalPage] = useState(1);
  const [search, setSearch] = useSearchParams();
  const [text, setText] = useState("");
  // const { data, isLoading } = useSearch(text, totalPage);

  const searchText = (item) => {
    setText(item.target.value);
    search.set("search", item.target.value);
    setSearch({ search: search.get("search") });
  };
  // useEffect(
  //   function changeInput() {
  //     setSearch({ search: text });
  //     console.log(search.get("search"));
  //   },
  //   [text, setSearch, search]
  // );

  // const clickLink = () => {
  //   window.scrollTo({
  //     top: 80
  //   });
  // };
  return (
    <>
      <section className="header-top">
        <div className="container-fluid">
          <div className="header-top_info">
            <div className="header-brand">
              <Link to="/">
                <img src={Brand} className="img-fluid" alt="" />
              </Link>
            </div>
            <div className="header-text">
              <p>
                Ushbu sayt elektrotexnika va elektronika bo'yicha ma’lumot
                beruvchi, amaliy va nazariy o’rgatuvchi sayt. Saytimiz yordamida
                siz nafaqat tushunasiz, elektrotexnika va elektronika sohasini
                yoqtirib ham qolasiz
              </p>
            </div>
          </div>
        </div>
      </section>
      <section className="header-bottom">
        <div className="container-fluid">
          <div className="navbar">
            <div className="nav-links">
              <Link exact to="/home" className="navLink">
                Bosh sahifa
              </Link>
              <Link exact to="/elektrotexnika" className="navLink">
                Elektrotexnika
              </Link>
              <Link exact to="/elektronika" className="navLink">
                Elektronika
              </Link>
              <Link exact to="/avtomatika" className="navLink">
                Avtomatika
              </Link>
              <Link exact to="/answer" className="navLink">
                Savol-javoblar
              </Link>
              <Link exact to="/books" className="navLink">
                Adabiyotlar
              </Link>
              {/* <Link to="/search" className="navLink">
                Qiriduv tizimiga kirish
              </Link> */}
            </div>
            <div className="nav-search">
              <Search
                onSearch={() => push("/search?search=" + search.get("search"))}
                type="text"
                placeholder="Qidiruv..."
                onChange={searchText.bind(this)}
                value={text}
                style={{ color: "#024f00" }}
              />
            </div>
          </div>
          <div className="navbarMobile">
            <div className="headerMenu">
              <div className="menuIcon" onClick={showDrawer}>
                <Menu />
              </div>
              <Link to="/">
                <img src={Logo} alt="" />
              </Link>
            </div>
            <Drawer
              title="Elcom"
              placement="right"
              onClose={onClose}
              visible={visible}
            >
              <Link exact to="/home" className="navLink" onClick={onClose}>
                Bosh sahifa
              </Link>
              <Link
                exact
                to="/elektrotexnika"
                className="navLink"
                onClick={onClose}
              >
                Elektrotexnika
              </Link>
              <Link
                exact
                to="/elektronika"
                className="navLink"
                onClick={onClose}
              >
                Elektronika
              </Link>
              <Link
                exact
                to="/avtomatika"
                className="navLink"
                onClick={onClose}
              >
                Avtomatika
              </Link>
              <Link exact to="/answer" className="navLink" onClick={onClose}>
                Savol-javoblar
              </Link>
              <Link exact to="/books" className="navLink" onClick={onClose}>
                Adabiyotlar
              </Link>
            </Drawer>
          </div>
          <div className="mobileSearch">
            <Search
              onSearch={() => push("/search?search=" + search.get("search"))}
              type="text"
              placeholder="Qidiruv..."
              onChange={searchText.bind(this)}
              value={text}
              style={{ color: "#024f00" }}
            />
          </div>
          {/* <div className="headerSecondText">
            <p>
              Ushbu sayt elektrotexnika va elektronika bo'yicha ma’lumot
              beruvchi, amaliy va nazariy o’rgatuvchi sayt. Saytimiz yordamida
              siz nafaqat tushunasiz, elektrotexnika va elektronika sohasini
              yoqtirib ham qolasiz
            </p>
          </div> */}
        </div>
      </section>
    </>
  );
}

export default Header;
