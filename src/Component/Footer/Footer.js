import { Col, Row, Spin } from "antd";
import React from "react";
import { useFooter } from "../../Querys/query";
import "./Footer.scss";
import "./Responsive.scss";
import logo from "./logo.png";
import { Link } from "react-router-dom";

function Footer() {
  const { data: footerData, isLoading: footerLoading } = useFooter();
  // const { data: footerImg, isLoading: footerLoadingImg } = useFooterImg();
  //   console.log(data);
  const clickLogo = () => {
    window.scrollTo(0, 0);
  };
  return (
    <footer className="footer">
      <div className="container-fluid">
        <Row justify="space-between" align="middle">
          <Col md={{ span: 8 }}>
            <Spin spinning={footerLoading}>
              <div className="footer-logo">
                <Link to="/" onClick={clickLogo}>
                  <img src={logo} alt="" />
                  <img className="footerLogo2" src={logo} alt="" />
                </Link>
              </div>
            </Spin>
          </Col>
          <Col md={{ span: 8 }}>
            <Spin spinning={footerLoading}>
              <div className="footer-email">
                <p
                  dangerouslySetInnerHTML={{
                    __html: footerData && footerData[3]?.title?.rendered
                  }}
                  className="footerEmailText"
                ></p>
                <a
                  href="https://elcominfo.uz@gmail.com"
                  dangerouslySetInnerHTML={{
                    __html: footerData && footerData[3].content.rendered
                  }}
                ></a>
              </div>
            </Spin>
          </Col>
          <Col md={{ span: 8 }}>
            <div className="footer-link">
              <p
                dangerouslySetInnerHTML={{
                  __html: footerData && footerData[2]?.title?.rendered
                }}
              ></p>
              {/* <a href="#">
                <img
                  src={
                    footerData &&
                    footerData[2]?._embedded["wp:featuredmedia"]?.map(
                      (link) => {
                        return link.source_url;
                      }
                    )
                  }
                  alt=""
                />
              </a> */}
              <a href="https://www.youtube.com/channel/UCb1F5CyKZ2R_ppfC8zwvHsQ">
                <img
                  src={
                    footerData &&
                    footerData[0]?._embedded["wp:featuredmedia"]?.map(
                      (link) => {
                        return link.source_url;
                      }
                    )
                  }
                  alt=""
                />
              </a>
              <a href="https://t.me/ELCOM_ELEKTRONIKA">
                <img
                  src={
                    footerData &&
                    footerData[1]?._embedded["wp:featuredmedia"]?.map(
                      (link) => {
                        return link.source_url;
                      }
                    )
                  }
                  alt=""
                />
              </a>
            </div>
          </Col>
        </Row>
        <p
          className="footer-text"
          dangerouslySetInnerHTML={{
            __html: footerData && footerData[2]?.content?.rendered
          }}
        ></p>
      </div>
    </footer>
  );
}

export default Footer;

// _embedded["wp:featuredmedia"].source_url
