import { Col, Row, Spin } from "antd";
import React from "react";
import { Link } from "react-router-dom";
import { Swiper, SwiperSlide } from "swiper/react";
import { useAnswer } from "../../Querys/query";
import "./Answer.scss";
import "swiper/css";

// import { Autoplay } from "swiper";
function Answer() {
  const { data: dataAnswer, isLoading: answerLoading } = useAnswer();
  // const { data: AnswerQuestion, isLoading: AnswerQuestionLoading } =
  //   useAnswerQuestion();
  // console.log(dataAnswer.data[0]);
  const clickLink = () => {
    window.scrollTo({
      top: 80
    });
  };
  return (
    <section className="answer">
      <div className="answer-info">
        <div className="answer-title">
          <h3 className="title" style={{ margin: 0 }}>
            <div className="title-circle bgRed"></div>
            Savol-javoblar
          </h3>
          <Link to="/answer" onClick={() => clickLink}>
            Barcha savollar...
          </Link>
        </div>
        <Row justify="space-between">
          <Col md={{ span: 6 }}>
            <Link
              to={`/answerPosts/${dataAnswer?.data[3]?.id}`}
              onClick={clickLink}
            >
              <div className="answer-card">
                <h5
                  dangerouslySetInnerHTML={{
                    __html:
                      dataAnswer &&
                      dataAnswer.data[3]?.title.rendered
                        .slice(0, 50)
                        .concat("...")
                  }}
                ></h5>
                <p
                  dangerouslySetInnerHTML={{
                    __html:
                      dataAnswer &&
                      dataAnswer.data[3]?.content.rendered
                        .slice(0, 235)
                        .concat("...")
                  }}
                ></p>
              </div>
            </Link>
          </Col>
          <Col md={{ span: 6 }}>
            <Link
              to={`/answerPosts/${dataAnswer?.data[2]?.id}`}
              onClick={clickLink}
            >
              <div className="answer-card">
                <h5
                  dangerouslySetInnerHTML={{
                    __html:
                      dataAnswer &&
                      dataAnswer.data[2]?.title.rendered
                        .slice(0, 50)
                        .concat("...")
                  }}
                ></h5>
                <p
                  dangerouslySetInnerHTML={{
                    __html:
                      dataAnswer &&
                      dataAnswer.data[2]?.content.rendered
                        .slice(0, 235)
                        .concat("...")
                  }}
                ></p>
              </div>
            </Link>
          </Col>
          <Col md={{ span: 6 }}>
            <Link
              to={`/answerPosts/${dataAnswer?.data[1]?.id}`}
              onClick={clickLink}
            >
              <div className="answer-card">
                <h5
                  dangerouslySetInnerHTML={{
                    __html:
                      dataAnswer &&
                      dataAnswer.data[1]?.title.rendered
                        .slice(0, 50)
                        .concat("...")
                  }}
                ></h5>
                <p
                  dangerouslySetInnerHTML={{
                    __html:
                      dataAnswer &&
                      dataAnswer.data[1]?.content.rendered
                        .slice(0, 235)
                        .concat("...")
                  }}
                ></p>
              </div>
            </Link>
          </Col>
          <Col md={{ span: 6 }}>
            <Link
              to={`/answerPosts/${dataAnswer?.data[0]?.id}`}
              onClick={clickLink}
            >
              <div className="answer-card">
                <h5
                  dangerouslySetInnerHTML={{
                    __html:
                      dataAnswer &&
                      dataAnswer.data[0]?.title.rendered
                        .slice(0, 50)
                        .concat("...")
                  }}
                ></h5>
                <p
                  dangerouslySetInnerHTML={{
                    __html:
                      dataAnswer &&
                      dataAnswer.data[0]?.content.rendered
                        .slice(0, 235)
                        .concat("...")
                  }}
                ></p>
              </div>
            </Link>
          </Col>
        </Row>
        <div className="answerSwiper">
          <Swiper
            slidesPerView={"auto"}
            spaceBetween={30}
            // autoplay={{
            //   delay: 2000,
            //   disableOnInteraction: false
            // }}
            // modules={[Autoplay]}
            className="mySwiper"
          >
            <SwiperSlide style={{ width: "80%" }}>
              <Spin spinning={answerLoading}>
                <div className="answer-card2">
                  <h5
                    dangerouslySetInnerHTML={{
                      __html:
                        dataAnswer &&
                        dataAnswer.data[3]?.title.rendered
                          .slice(0, 50)
                          .concat("...")
                    }}
                  ></h5>
                  <p
                    dangerouslySetInnerHTML={{
                      __html:
                        dataAnswer &&
                        dataAnswer.data[3]?.content.rendered
                          .slice(0, 235)
                          .concat("...")
                    }}
                  ></p>
                </div>
              </Spin>
            </SwiperSlide>
            <SwiperSlide style={{ width: "100%" }}>
              <Spin spinning={answerLoading}>
                <div className="answer-card2">
                  <h5
                    dangerouslySetInnerHTML={{
                      __html:
                        dataAnswer &&
                        dataAnswer.data[2]?.title.rendered
                          .slice(0, 50)
                          .concat("...")
                    }}
                  ></h5>
                  <p
                    dangerouslySetInnerHTML={{
                      __html:
                        dataAnswer &&
                        dataAnswer.data[2]?.content.rendered
                          .slice(0, 235)
                          .concat("...")
                    }}
                  ></p>
                </div>
              </Spin>
            </SwiperSlide>
            <SwiperSlide style={{ width: "100%" }}>
              <Spin spinning={answerLoading}>
                <div className="answer-card2">
                  <h5
                    dangerouslySetInnerHTML={{
                      __html:
                        dataAnswer &&
                        dataAnswer.data[1]?.title.rendered
                          .slice(0, 50)
                          .concat("...")
                    }}
                  ></h5>
                  <p
                    dangerouslySetInnerHTML={{
                      __html:
                        dataAnswer &&
                        dataAnswer.data[1]?.content.rendered
                          .slice(0, 235)
                          .concat("...")
                    }}
                  ></p>
                </div>
              </Spin>
            </SwiperSlide>
            <SwiperSlide style={{ width: "100%" }}>
              <Spin spinning={answerLoading}>
                <div className="answer-card2">
                  <h5
                    dangerouslySetInnerHTML={{
                      __html:
                        dataAnswer &&
                        dataAnswer.data[0]?.title.rendered
                          .slice(0, 50)
                          .concat("...")
                    }}
                  ></h5>
                  <p
                    dangerouslySetInnerHTML={{
                      __html:
                        dataAnswer &&
                        dataAnswer.data[0]?.content.rendered
                          .slice(0, 235)
                          .concat("...")
                    }}
                  ></p>
                </div>
              </Spin>
            </SwiperSlide>
          </Swiper>
        </div>
      </div>
    </section>
  );
}

export default Answer;
