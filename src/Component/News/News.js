import { Col, Row } from "antd";
import React from "react";
import { Link } from "react-router-dom";
import { useNews } from "../../Querys/query";
import "./News.scss";

function News() {
  const { data: newsData, isLoading: newsLoading } = useNews();
  // const { data: dataImg, isLoading: imgLoading } = useImages();
  // console.log(newsData);
  // console.log(dataImg);
  const clickLink = () => {
    window.scrollTo({
      top: 80
    });
  };
  return (
    <section className="newsHome">
      <div className="container-fluid">
        <div className="news-card">
          <Row id="wh100">
            <Col span={12}>
              <div className="news_active">
                <Link
                  to={`/news/${newsData && newsData[0].id}`}
                  className="newsLink"
                  id="wh100"
                  onClick={clickLink}
                >
                  {newsLoading ? (
                    <p></p>
                  ) : (
                    <h3
                      dangerouslySetInnerHTML={{
                        __html: newsData && newsData[0].title.rendered
                      }}
                    ></h3>
                  )}
                  {/* {dataImg?.map(
                      (i) =>
                        i.post === newsData[3].id && (
                          <img src={i.source_url} alt="" />
                        )
                    )} */}
                  <img
                    src={
                      newsData &&
                      newsData[0]?._embedded["wp:featuredmedia"]?.map((img) => {
                        return img.media_details.sizes.full.source_url;
                      })
                    }
                    alt=""
                  />
                  <div className="news-date">
                    <p
                      dangerouslySetInnerHTML={{
                        __html:
                          newsData &&
                          newsData[0].date
                            ?.replace(/-/g, ".")
                            .slice(0, 10)
                            .split(".")
                            .reverse()
                            .join(".")
                      }}
                    ></p>
                    <p
                      dangerouslySetInnerHTML={{
                        __html: newsData && newsData[0].excerpt.rendered
                      }}
                    ></p>
                  </div>
                </Link>
              </div>
            </Col>
            <Col span={12}>
              <div id="wh100" className="newsContainer">
                <div className="news-items">
                  <Link
                    to={`/news/${newsData && newsData[1].id}`}
                    id="wh100"
                    className="newsLink2"
                    onClick={clickLink}
                  >
                    {/* <Spin spinning={newsLoading} id="wh100"> */}
                    <img
                      src={
                        newsData &&
                        newsData[1]?._embedded["wp:featuredmedia"]?.map(
                          (img) => {
                            return img.media_details.sizes.full.source_url;
                          }
                        )
                      }
                      alt=""
                    />
                    <div className="news-item">
                      {newsLoading ? (
                        <p></p>
                      ) : (
                        <h3
                          dangerouslySetInnerHTML={{
                            __html: newsData && newsData[1].title.rendered
                          }}
                        ></h3>
                      )}
                      <div className="news-date">
                        <p
                          dangerouslySetInnerHTML={{
                            __html:
                              newsData &&
                              newsData[1].date
                                ?.replace(/-/g, ".")
                                .slice(0, 10)
                                .split(".")
                                .reverse()
                                .join(".")
                          }}
                        ></p>
                        <p
                          dangerouslySetInnerHTML={{
                            __html: newsData && newsData[1].excerpt.rendered
                          }}
                        ></p>
                      </div>
                    </div>
                    {/* </Spin> */}
                  </Link>
                </div>
                <div className="news-items_card_second">
                  <div className="news-items_second">
                    <Link
                      to={`/news/${newsData && newsData[2].id}`}
                      id="wh100"
                      className="newsLink3"
                      onClick={clickLink}
                    >
                      <img
                        src={
                          newsData &&
                          newsData[2]?._embedded["wp:featuredmedia"]?.map(
                            (img) => {
                              return img.media_details.sizes.full.source_url;
                            }
                          )
                        }
                        alt=""
                      />
                      <div className="news-item_second">
                        {newsLoading ? (
                          <p></p>
                        ) : (
                          <h3
                            dangerouslySetInnerHTML={{
                              __html: newsData && newsData[2].title.rendered
                            }}
                          ></h3>
                        )}
                        <div className="news-date">
                          <p
                            dangerouslySetInnerHTML={{
                              __html:
                                newsData &&
                                newsData[2].date
                                  ?.replace(/-/g, ".")
                                  .slice(0, 10)
                                  .split(".")
                                  .reverse()
                                  .join(".")
                            }}
                          ></p>
                          <p
                            dangerouslySetInnerHTML={{
                              __html: newsData && newsData[2].excerpt.rendered
                            }}
                          ></p>
                        </div>
                      </div>
                    </Link>
                  </div>
                  <div className="news-items_second">
                    <Link
                      to={`/news/${newsData && newsData[3].id}`}
                      id="wh100"
                      className="newsLink3"
                      onClick={clickLink}
                    >
                      <img
                        src={
                          newsData &&
                          newsData[3]?._embedded["wp:featuredmedia"]?.map(
                            (img) => {
                              return img.media_details.sizes.full.source_url;
                            }
                          )
                        }
                        alt=""
                      />
                      <div className="news-item_second">
                        {newsLoading ? (
                          <p></p>
                        ) : (
                          <h3
                            dangerouslySetInnerHTML={{
                              __html: newsData && newsData[3].title.rendered
                            }}
                          ></h3>
                        )}
                        <div className="news-date">
                          <p
                            dangerouslySetInnerHTML={{
                              __html:
                                newsData &&
                                newsData[3].date
                                  ?.replace(/-/g, ".")
                                  .slice(0, 10)
                                  .split(".")
                                  .reverse()
                                  .join(".")
                            }}
                          ></p>
                          <p
                            dangerouslySetInnerHTML={{
                              __html: newsData && newsData[3].excerpt.rendered
                            }}
                          ></p>
                        </div>
                      </div>
                    </Link>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </section>
  );
}

export default News;
