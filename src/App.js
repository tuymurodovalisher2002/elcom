import "./App.scss";
import "./Animate.scss";
import { QueryClient, QueryClientProvider } from "react-query";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Header from "./Component/Header/Header";
import "antd/dist/antd.css";
import "antd/dist/antd.variable.min.css";
import Footer from "./Component/Footer/Footer";
import AnswerQuestion from "./AnswerQuestion/AnswerQuestion";
import Home from "./Home/Home";
import Elektrotexnika from "./Elektrotexnika/Elektrotexnika";
import Elektronika from "./Elektronika/Elektronika";
import Avtomatika from "./Avtomatika/Avtomatika";
import Books from "./Books/Books";
import NewsFirst from "./News/NewsFirst";
// import NewsSecond from "./News/NewsSecond";
// import NewsThirth from "./News/NewsThirth";
// import NewsForth from "./News/NewsFourth";
import Articles from "./Articles/Articles";
import { ConfigProvider } from "antd";
import AnswerPost from "./AnswerQuestion/AnswerPost";
import SearchPost from "./SearchPost/SearchPost";
// import { Helmet } from "react-helmet";
import { HelmetProvider } from "react-helmet-async";

const queryClient = new QueryClient();
ConfigProvider.config({
  theme: {
    primaryColor: "#024f00"
  }
});
function App() {
  return (
    <HelmetProvider>
      <QueryClientProvider client={queryClient}>
        <BrowserRouter>
          <div className="App">
            <Header />
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/home" element={<Home />} />
              <Route path="/elektrotexnika" element={<Elektrotexnika />} />
              <Route path="/elektronika" element={<Elektronika />} />
              <Route path="/avtomatika" element={<Avtomatika />} />
              <Route path="/answer" element={<AnswerQuestion />} />
              <Route path="/books" element={<Books />} />
              <Route path="/news/:id" element={<NewsFirst />} />
              {/* <Route path="/news2" element={<NewsSecond />} />
            <Route path="/news3" element={<NewsThirth />} />
            <Route path="/news4" element={<NewsForth />} /> */}
              <Route path="/posts/:id" element={<Articles />} />
              <Route path="/answerPosts/:id" element={<AnswerPost />} />
              <Route path="/search" element={<SearchPost />} />
            </Routes>
            <Footer />
          </div>
        </BrowserRouter>
      </QueryClientProvider>
    </HelmetProvider>
  );
}

export default App;
