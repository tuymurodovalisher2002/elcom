import React from "react";
import DateIcon from "../Elektrotexnika/DateIcon";

function SearchCard(props) {
  return (
    <div className="searchCard">
      <div className="searchDate">
        <h5 dangerouslySetInnerHTML={{ __html: props.name }}></h5>
        <p>
          <DateIcon />
          {props.date}
        </p>
      </div>
      <div className="searchText">
        <h3 dangerouslySetInnerHTML={{ __html: props.title }}></h3>
        <p dangerouslySetInnerHTML={{ __html: props.text }}></p>
      </div>
    </div>
  );
}

export default SearchCard;
