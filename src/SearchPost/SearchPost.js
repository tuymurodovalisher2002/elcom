import { Col, Empty, Input, Pagination, Row, Spin } from "antd";
import React, { useEffect, useState } from "react";
import { useSearch } from "../Querys/query";
import "./SearchPost.scss";
import "./Responsive.scss";
import SearchCard from "./SearchCard";
import NewsArticle from "../Home/NewsArticle";
import { Link, useSearchParams } from "react-router-dom";
import Helmet from "react-helmet";
// const { Search } = Input;

function SearchPost() {
  const [totalPage, setTotalPage] = useState(1);
  const [search, setSearch] = useSearchParams();
  const [text, setText] = useState("");
  const { data, isLoading } = useSearch(search.get("search"), totalPage);

  console.log(data);
  // search.set("search",text)
  // const searchText = (item) => {
  //   setText(item.target.value);
  // };

  // useEffect(() => {
  //   setText(search.get("search"))
  //   setSearch({ search: text });
  //   console.log(search.get("search"));
  // }, [text, setSearch, search]);
  const clickLink = () => {
    window.scrollTo({
      top: 80
    });
  };
  return (
    <>
      <Helmet>
        <title>Elcom - Qidiruv tizimi</title>
        <meta
          name="description"
          content="Elcom qidiruv tizimi. Barcha ma'lumotlarni qidirish. Bu yerda uzingizga kerakli adabiyotlarni, maqolalarni va savollarga javob olishingiz mumkin"
        />
        <meta
          name="keywords"
          content="Elcom qidiruv tizimi. Maqolalarni, Adabiyotlarni qidirish"
        />
        <meta
          property="og:title"
          content="Elcom qidiruv tizimi. Maqolalarni, Adabiyotlarni qidirish"
        />
        <meta
          property="og:description"
          content="Elcom qidiruv tizimi. Barcha ma'lumotlarni qidirish. Bu yerda uzingizga kerakli adabiyotlarni, maqolalarni va savollarga javob olishingiz mumkin"
        />
        <meta property="og:type" content="website" />
        <meta
          property="og:url"
          content={`https://elcominfo.uz/search?search=`}
        />
        <meta property="og:site_name" content="elcominfo.uz" />
        <meta
          property="og:image"
          content="https://admin.elcominfo.uz/wp-content/uploads/2022/04/facicon-1.png"
        />
        <meta
          type="image/png"
          name="link"
          href="https://admin.elcominfo.uz/wp-content/uploads/2022/04/facicon-1.png"
        />
      </Helmet>
      <section className="searchPost">
        <div className="container-fluid">
          <Row>
            <Col md={{ span: 14 }} style={{ paddingRight: "30px" }}>
              {/* <Search
                type="text"
                placeholder="Qidiruv..."
                onChange={searchText.bind(this)}
                value={text}
                style={{ height: 50, color: "#024f00" }}
              /> */}
              <Spin spinning={isLoading}>
                {data?.data.length >= 1 ? (
                  data?.data.map((item) => (
                    <Link
                      to={
                        item?.categories[0] === 14 ||
                        item?.categories[0] === 21 ||
                        item?.categories[0] === 22 ||
                        item?.categories[0] === 23
                          ? `/answerPosts/${item?.id}`
                          : `/posts/${item?.id}`
                      }
                      onClick={clickLink}
                    >
                      <SearchCard
                        name={
                          item?.excerpt.rendered === ""
                            ? item?.excerpt.rendered + "Adabiyotlar"
                            : item?.excerpt.rendered
                        }
                        date={item?.date
                          ?.replace(/-/g, ".")
                          .slice(0, 10)
                          .split(".")
                          .reverse()
                          .join(".")}
                        title={item?.title.rendered}
                        text={item?.content.rendered.slice(0, 380)}
                      />
                    </Link>
                  ))
                ) : (
                  <Empty description={"Ma'lumot mavjud emas !"} />
                )}
              </Spin>
              <div className="paginations">
                {/* <Pagination
                defaultCurrent={1}
                total={Number(data?.headers["x-wp-total"])}
                onChange={(page) => setTotalPage(page)}
              /> */}
                {data?.headers["x-wp-total"] > 10 ? (
                  <Pagination
                    defaultCurrent={1}
                    total={Number(data?.headers["x-wp-total"])}
                    onChange={(page) => setTotalPage(page)}
                  />
                ) : null}
              </div>
            </Col>
            <Col md={{ span: 10 }} style={{ paddingLeft: "20px" }}>
              <NewsArticle />
            </Col>
          </Row>
        </div>
      </section>
    </>
  );
}

export default SearchPost;
