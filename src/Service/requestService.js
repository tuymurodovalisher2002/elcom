import axios from "axios";

const $api = axios.create({
    baseURL: "https://admin.elcominfo.uz/wp-json/wp/v2"
})

export default $api