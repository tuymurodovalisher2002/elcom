import { Row, Col } from "antd";
import React from "react";
import Answer from "../Component/Answer/Answer";
import News from "../Component/News/News";
import NewsArticle from "./NewsArticle";
import "./Home.scss";
import "./Responsive.scss";
import NewElektrotexnika from "./NewElektrotexnika";
import { Helmet } from "react-helmet-async";

function Home() {
  return (
    <>
      <Helmet>
        <title>Elcominfo</title>
        <meta
          name="description"
          content="Energetika, Elektr energetikasi, Elektronika, Elektrotexnika va Avtomatika kabi sohalar haqida malumot olishingiz"
        />
        <link rel="canonical" href="/home" />
        <meta name="robots" content="index,follow" />
        <meta
          name="keywords"
          content="Elcom, Energetika, Elektronika, Elektrotexnika, Avtomatika, Elcominfo, Elcom Darslar, Elcom savol-javob"
        />
        <meta
          type="image/png"
          name="link"
          href="https://admin.elcominfo.uz/wp-content/uploads/2022/04/facicon-1.png"
        />

        {/* OGP SEO */}

        <meta
          property="og:title"
          content="elcominfo.uz.Energetika sohasidagi qiziqarli maqolalar"
        />
        <meta
          property="og:description"
          content="Eng qiziqarli yangiliklar — bizda! Energetika, Elektronika, Elektrotexnika, Avtomatika, Elektr energetikasi olami yangiliklaridan xabardor bo‘ling."
        />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://elcominfo.uz/" />
        <meta property="og:site_name" content="elcominfo.uz" />
        <meta
          property="og:image"
          content="https://admin.elcominfo.uz/wp-content/uploads/2022/04/facicon-1.png"
        />
        <meta property="og:image:witdh" content="1200" />
        <meta property="og:image:height" content="630" />
        <meta property="og:locale" content="uz_uz" />

        {/* Twitter Card SEO  */}

        <meta name="twitter:title" content="Elcominfo " />
        <meta
          name="twitter:description"
          content="Energetika, Elektr energetikasi, Elektronika, Elektrotexnika va Avtomatika kabi sohalar haqida malumot olishingiz."
        />
        <meta
          name="twitter:image"
          content=" https://admin.elcominfo.uz/wp-content/uploads/2022/04/facicon-1.png"
        />
        <meta name="twitter:card" content="summary_large_image" />
      </Helmet>
      <section className="home">
        <div className="container-fluid">
          <News />
          <Row>
            <Col md={{ span: 14 }}>
              <NewsArticle />
            </Col>
            <Col md={{ span: 10 }}>
              <NewElektrotexnika />
            </Col>
          </Row>
          <Answer />
        </div>
      </section>
    </>
  );
}

export default Home;
