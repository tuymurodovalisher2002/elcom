import React from "react";
import { Link } from "react-router-dom";
import { useElektrotexnika } from "../Querys/query";
import "./Home.scss";

function NewElektrotexnika() {
  const pageNumber = 1;
  const { data: ElektrotexnikaData } = useElektrotexnika(pageNumber);
  const clickLink = () => {
    window.scrollTo({
      top: 80
    });
  };
  return (
    <section className="newElektrotexnika">
      <div className="title">
        <div className="title-circle"></div>Elektrotexnika
        <div className="title-new">New</div>
      </div>
      <Link to={`/news/${ElektrotexnikaData?.data[0]?.id}`} onClick={clickLink}>
        <div className="newElektrotexnikaCardActive">
          <img
            src={ElektrotexnikaData?.data[0]?._embedded[
              "wp:featuredmedia"
            ]?.map((img) => {
              return img.source_url;
            })}
            alt=""
          />
          <div>
            <h5>Elektrotexnika</h5>
            <p>
              {ElektrotexnikaData?.data[0]?.date
                ?.replace(/-/g, ".")
                .slice(0, 10)
                .split(".")
                .reverse()
                .join(".")}
            </p>
          </div>
          <h3
            dangerouslySetInnerHTML={{
              __html: ElektrotexnikaData?.data[0]?.title.rendered
                .slice(0, 100)
                .concat("...")
            }}
          ></h3>
          <p
            dangerouslySetInnerHTML={{
              __html: ElektrotexnikaData?.data[0]?.content.rendered
                .slice(0, 200)
                .concat("...")
            }}
          ></p>
        </div>
      </Link>
      <Link to={`/news/${ElektrotexnikaData?.data[1]?.id}`} onClick={clickLink}>
        <div className="newElektrotexnikaCard">
          <div className="newElektrotexnikaCardText">
            <h4
              dangerouslySetInnerHTML={{
                __html: ElektrotexnikaData?.data[1]?.title.rendered
                  .slice(0, 100)
                  .concat("...")
              }}
            ></h4>
            <p>
              {ElektrotexnikaData?.data[1]?.date
                ?.replace(/-/g, ".")
                .slice(0, 10)
                .split(".")
                .reverse()
                .join(".")}
            </p>
          </div>
          <div className="newElektrotexnikaCardImg">
            <div>
              <img
                src={ElektrotexnikaData?.data[1]?._embedded[
                  "wp:featuredmedia"
                ]?.map((img) => {
                  return img.source_url;
                })}
                alt=""
              />
            </div>
          </div>
        </div>
      </Link>
      <Link to={`/news/${ElektrotexnikaData?.data[2]?.id}`} onClick={clickLink}>
        <div className="newElektrotexnikaCard">
          <div className="newElektrotexnikaCardText">
            <h4
              dangerouslySetInnerHTML={{
                __html: ElektrotexnikaData?.data[2]?.title.rendered
                  .slice(0, 100)
                  .concat("...")
              }}
            ></h4>
            <p>
              {ElektrotexnikaData?.data[2]?.date
                ?.replace(/-/g, ".")
                .slice(0, 10)
                .split(".")
                .reverse()
                .join(".")}
            </p>
          </div>
          <div className="newElektrotexnikaCardImg">
            <div>
              <img
                src={ElektrotexnikaData?.data[2]?._embedded[
                  "wp:featuredmedia"
                ]?.map((img) => {
                  return img.source_url;
                })}
                alt=""
              />
            </div>
          </div>
        </div>
      </Link>
      <Link to={`/news/${ElektrotexnikaData?.data[3]?.id}`} onClick={clickLink}>
        <div className="newElektrotexnikaCard">
          <div className="newElektrotexnikaCardText">
            <h4
              dangerouslySetInnerHTML={{
                __html: ElektrotexnikaData?.data[3]?.title.rendered
                  .slice(0, 100)
                  .concat("...")
              }}
            ></h4>
            <p>
              {ElektrotexnikaData?.data[3]?.date
                ?.replace(/-/g, ".")
                .slice(0, 10)
                .split(".")
                .reverse()
                .join(".")}
            </p>
          </div>
          <div className="newElektrotexnikaCardImg">
            <div>
              <img
                src={ElektrotexnikaData?.data[3]?._embedded[
                  "wp:featuredmedia"
                ]?.map((img) => {
                  return img.source_url;
                })}
                alt=""
              />
            </div>
          </div>
        </div>
      </Link>
    </section>
  );
}

export default NewElektrotexnika;
