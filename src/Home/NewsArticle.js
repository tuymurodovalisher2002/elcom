import React from "react";
import { Link } from "react-router-dom";
// import Elektrotexnika from "../Elektrotexnika/Elektrotexnika";
import {
  useActomatika,
  useElektronika,
  useElektrotexnika
} from "../Querys/query";

function NewsArticle(props) {
  const pageNumber = 1;
  const { data: ElektrotexnikaData } = useElektrotexnika(pageNumber);
  const { data: ElektronikaData } = useElektronika(pageNumber);
  const { data: AvtomatikaData } = useActomatika(pageNumber);
  // console.log(
  //   ElektrotexnikaData?.data[0]?.date
  //     ?.replace(/-/g, ".")
  //     .slice(0, 10)
  //     .split(".")
  //     .reverse()
  // );
  // const [num, setNum] = useState(200);
  const clickLink = () => {
    window.scrollTo({
      top: 80
    });
  };
  return (
    <section className="newArticle">
      <div className="title">
        <div className="title-circle"></div>Yangi maqolalar
      </div>
      <Link
        to={`/posts/${ElektrotexnikaData?.data[0]?.id}`}
        onClick={clickLink}
      >
        <div className="newArticleCardActive">
          <div className="newArticleCardActiveText">
            <div>
              <h5>Elektrotexnika</h5>
              <p>
                {ElektrotexnikaData?.data[0]?.date
                  ?.replace(/-/g, ".")
                  .slice(0, 10)
                  .split(".")
                  .reverse()
                  .join(".")}
              </p>
            </div>
            <h3
              dangerouslySetInnerHTML={{
                __html: ElektrotexnikaData?.data[0]?.title.rendered
              }}
            ></h3>
            <p
              dangerouslySetInnerHTML={{
                __html: ElektrotexnikaData?.data[0]?.content.rendered
                  .slice(0, 450)
                  .concat("...")
              }}
            ></p>
          </div>
          <div className="newArticleCardActiveImg">
            <div>
              <img
                src={ElektrotexnikaData?.data[0]?._embedded[
                  "wp:featuredmedia"
                ]?.map((img) => {
                  return img.source_url;
                })}
                alt=""
              />
            </div>
          </div>
        </div>
      </Link>
      <Link to={`/posts/${ElektronikaData?.data[0]?.id}`} onClick={clickLink}>
        <div className="newArticleCard">
          <div className="newArticleCardImg">
            <div>
              <img
                src={ElektronikaData?.data[0]?._embedded[
                  "wp:featuredmedia"
                ]?.map((img) => {
                  return img.source_url;
                })}
                alt=""
              />
            </div>
          </div>
          <div className="newArticleCardText">
            <div>
              <h5>Elektronika</h5>
              <p>
                {ElektronikaData?.data[0]?.date
                  ?.replace(/-/g, ".")
                  .slice(0, 10)
                  .split(".")
                  .reverse()
                  .join(".")}
              </p>
            </div>
            <h3
              dangerouslySetInnerHTML={{
                __html: ElektronikaData?.data[0]?.title.rendered
              }}
            ></h3>
            <p
              dangerouslySetInnerHTML={{
                __html: ElektronikaData?.data[0]?.content.rendered
                  .slice(0, 150)
                  .concat("...")
              }}
            ></p>
          </div>
        </div>
      </Link>
      <Link to={`/posts/${AvtomatikaData?.data[0]?.id}`} onClick={clickLink}>
        <div className="newArticleCard">
          <div className="newArticleCardImg">
            <div>
              <img
                src={AvtomatikaData?.data[0]?._embedded[
                  "wp:featuredmedia"
                ]?.map((img) => {
                  return img.source_url;
                })}
                alt=""
              />
            </div>
          </div>
          <div className="newArticleCardText">
            <div>
              <h5>Avtomatika</h5>
              <p>
                {AvtomatikaData?.data[0]?.date
                  ?.replace(/-/g, ".")
                  .slice(0, 10)
                  .split(".")
                  .reverse()
                  .join(".")}
              </p>
            </div>
            <h3
              dangerouslySetInnerHTML={{
                __html: AvtomatikaData?.data[0]?.title.rendered
              }}
            ></h3>
            <p
              dangerouslySetInnerHTML={{
                __html: AvtomatikaData?.data[0]?.content.rendered
                  .slice(0, 150)
                  .concat("...")
              }}
            ></p>
          </div>
        </div>
      </Link>
      <Link
        to={`/posts/${ElektrotexnikaData?.data[1]?.id}`}
        onClick={clickLink}
      >
        <div className="newArticleCard">
          <div className="newArticleCardImg">
            <div>
              <img
                src={ElektrotexnikaData?.data[1]?._embedded[
                  "wp:featuredmedia"
                ]?.map((img) => {
                  return img.source_url;
                })}
                alt=""
              />
            </div>
          </div>
          <div className="newArticleCardText">
            <div>
              <h5>Elektrotexnika</h5>
              <p>
                {ElektrotexnikaData?.data[1]?.date
                  ?.replace(/-/g, ".")
                  .slice(0, 10)
                  .split(".")
                  .reverse()
                  .join(".")}
              </p>
            </div>
            <h3
              dangerouslySetInnerHTML={{
                __html: ElektrotexnikaData?.data[1]?.title.rendered
              }}
            ></h3>
            <p
              dangerouslySetInnerHTML={{
                __html: ElektrotexnikaData?.data[1]?.content.rendered
                  .slice(0, 150)
                  .concat("...")
              }}
            ></p>
          </div>
        </div>
      </Link>
      <Link to={`/posts/${ElektronikaData?.data[1]?.id}`} onClick={clickLink}>
        <div className="newArticleCard">
          <div className="newArticleCardImg">
            <div>
              <img
                src={ElektronikaData?.data[1]?._embedded[
                  "wp:featuredmedia"
                ]?.map((img) => {
                  return img.source_url;
                })}
                alt=""
              />
            </div>
          </div>
          <div className="newArticleCardText">
            <div>
              <h5>Elektronika</h5>
              <p>
                {ElektronikaData?.data[1]?.date
                  ?.replace(/-/g, ".")
                  .slice(0, 10)
                  .split(".")
                  .reverse()
                  .join(".")}
              </p>
            </div>
            <h3
              dangerouslySetInnerHTML={{
                __html: ElektronikaData?.data[1]?.title.rendered
              }}
            ></h3>
            <p
              dangerouslySetInnerHTML={{
                __html: ElektronikaData?.data[1]?.content.rendered
                  .slice(0, 150)
                  .concat("...")
              }}
            ></p>
          </div>
        </div>
      </Link>
      <Link to={`/posts/${AvtomatikaData?.data[1]?.id}`} onClick={clickLink}>
        <div className="newArticleCard">
          <div className="newArticleCardImg">
            <div>
              <img
                src={AvtomatikaData?.data[1]?._embedded[
                  "wp:featuredmedia"
                ]?.map((img) => {
                  return img.source_url;
                })}
                alt=""
              />
            </div>
          </div>
          <div className="newArticleCardText">
            <div>
              <h5>Avtomatika</h5>
              <p>
                {AvtomatikaData?.data[1]?.date
                  ?.replace(/-/g, ".")
                  .slice(0, 10)
                  .split(".")
                  .reverse()
                  .join(".")}
              </p>
            </div>
            <h3
              dangerouslySetInnerHTML={{
                __html: AvtomatikaData?.data[1]?.title.rendered
              }}
            ></h3>
            <p
              dangerouslySetInnerHTML={{
                __html: AvtomatikaData?.data[1]?.content.rendered
                  .slice(0, 150)
                  .concat("...")
              }}
            ></p>
          </div>
        </div>
      </Link>
      <div className="newArticleLink">
        <Link to="/elektrotexnika" onClick={clickLink}>
          Barchasini ko'rish
        </Link>
      </div>
    </section>
  );
}

export default NewsArticle;
