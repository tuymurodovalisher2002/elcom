import { Col, Pagination, Row, Spin } from "antd";
import React, { useState } from "react";
import { useElektrotexnika } from "../Querys/query";
import ElektrotexnikaCard from "./ElektrotexnikaCard";
import "./Elektrotexnika.scss";
import { Link } from "react-router-dom";
import NewElektronika from "./NewElektronika";
import "./Responsive.scss";
import { Helmet } from "react-helmet-async";

function Elektrotexnika() {
  // function onShowSizeChange(current, pageSize) {
  //   console.log(current, pageSize);
  // }
  const [totalPage, setTotalPage] = useState(1);
  const { data, isLoading } = useElektrotexnika(totalPage);
  // console.log(data);
  const clickLink = () => {
    window.scrollTo({
      top: 80
    });
  };
  return (
    <>
      <Helmet>
        <title>Elcom - Elektrotexnika</title>
        <meta
          name="description"
          content="Elektrotexnika, Elektrotexnika doir qiziqarli maqolalar, Elektrotexnika qurilmalari, Elektrotexnika adabiyotlar"
        />
        <link rel="canonical" href="/elektrotexnika" />
        <meta name="robots" content="index,follow" />
        <meta
          name="keywords"
          content="Elcom Elektrotexnika, Elektrotexnika, Elektrotexnika maqola, Elektrotexnika kitob, Elektrotexnika savollar"
        />
        <meta
          property="og:title"
          content="Elektrotexnika Elektrotexnika maqola, Elektrotexnika kitob, Elektrotexnika savollar"
        />
        <meta
          property="og:description"
          content="Elektrotexnika, Elektrotexnika doir qiziqarli maqolalar, Elektrotexnika qurilmalari, Elektrotexnika adabiyotlar, Elcom Elektrotexnika"
        />
        <meta property="og:type" content="website" />
        <meta
          property="og:url"
          content={`https://elcominfo.uz/elektrotexnika`}
        />
        <meta property="og:site_name" content="elcominfo.uz" />
        <meta
          property="og:image"
          content="https://admin.elcominfo.uz/wp-content/uploads/2022/04/facicon-1.png"
        />
        <meta
          type="image/png"
          name="link"
          href="https://admin.elcominfo.uz/wp-content/uploads/2022/04/facicon-1.png"
        />
        <meta property="og:image:witdh" content="200" />
        <meta property="og:image:height" content="200" />
        <meta property="og:image:alt" content="Elcom Elektrotexnika" />
        <meta property="og:locale" content="uz_uz" />

        {/* Twitter Card SEO  */}

        <meta
          name="twitter:title"
          content="Elektrotexnika Elektrotexnika maqola, Elektrotexnika kitob, Elektrotexnika savollar."
        />
        <meta
          name="twitter:description"
          content="Elektrotexnika, Elektrotexnika doir qiziqarli maqolalar, Elektrotexnika qurilmalari, Elektrotexnika adabiyotlar."
        />
        <meta
          name="twitter:image"
          content=" https://admin.elcominfo.uz/wp-content/uploads/2022/04/facicon-1.png"
        />
        <meta name="twitter:card" content="summary_large_image" />
      </Helmet>
      <section className="elektrotexnika">
        <div className="container-fluid">
          <Row>
            <Col md={{ span: 14 }} style={{ paddingRight: "30px" }}>
              <h3 className="title-large">Elektrotexnika</h3>
              <Spin spinning={isLoading}>
                {data?.data?.map((data) => {
                  return (
                    <Link to={`/posts/${data.id}`} onClick={clickLink}>
                      <ElektrotexnikaCard
                        title={data?.title?.rendered.slice(0, 40).concat("...")}
                        text={data.content.rendered.slice(0, 380).concat("...")}
                        img={data?._embedded["wp:featuredmedia"]?.map((img) => {
                          return img.source_url;
                        })}
                        date={data.date
                          ?.replace(/-/g, ".")
                          .slice(0, 10)
                          .split(".")
                          .reverse()
                          .join(".")}
                        // author={data?._embedded["wp:featuredmedia"]?.map(
                        //   (author) => {
                        //     return author.caption.rendered;
                        //   }
                        // )}
                      />
                    </Link>
                  );
                })}
              </Spin>
              <div className="paginations">
                {data?.headers["x-wp-total"] > 10 ? (
                  <Pagination
                    defaultCurrent={1}
                    total={Number(data?.headers["x-wp-total"])}
                    onChange={(page) => setTotalPage(page)}
                  />
                ) : null}
              </div>
            </Col>
            <Col md={{ span: 10 }}>
              <NewElektronika />
            </Col>
          </Row>
        </div>
      </section>
    </>
  );
}

export default Elektrotexnika;
