import { React } from "react";
import DateIcon from "./DateIcon";
import eye from "./Eye.png";

function ElektrotexnikaCard(props) {
  return (
    <div className="electrotexnikaCard">
      <div className="electro-card_img">
        <div>
          <img src={props.img} alt="" />
        </div>
      </div>
      <div className="electro-card_info">
        <h5 dangerouslySetInnerHTML={{ __html: props.title }}></h5>
        <div className="electro-card_date">
          <p>
            <span>
              {/* <img src={eye} alt="" /> */}
              <DateIcon />
              {props.date}
            </span>
            <span dangerouslySetInnerHTML={{ __html: props.author }}></span>
          </p>
        </div>
        <p
          className="electro-card_text"
          dangerouslySetInnerHTML={{ __html: props.text }}
        ></p>
      </div>
      <div></div>
    </div>
  );
}

export default ElektrotexnikaCard;
