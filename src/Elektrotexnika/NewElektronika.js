import React from "react";
import { Link } from "react-router-dom";
import { useElektronika } from "../Querys/query";

function NewElektronika() {
  const pageNumber = 1;
  const { data: ElektronikaData } = useElektronika(pageNumber);
  console.log(ElektronikaData);
  const clickLink = () => {
    window.scrollTo({
      top: 80
    });
  };
  return (
    <section className="newElektrotexnika">
      <div className="title">
        <div className="title-circle"></div>Elektronika
        <div className="title-new">New</div>
      </div>
      <Link to={`/news/${ElektronikaData?.data[0]?.id}`} onClick={clickLink}>
        <div className="newElektrotexnikaCardActive">
          <img
            src={ElektronikaData?.data[0]?._embedded["wp:featuredmedia"]?.map(
              (img) => {
                return img.source_url;
              }
            )}
            alt=""
          />
          <div>
            <h5>Elektronika</h5>
            <p>
              {ElektronikaData &&
                ElektronikaData?.data[0]?.date
                  ?.replace(/-/g, ".")
                  .slice(0, 10)
                  .split(".")
                  .reverse()
                  .join(".")}
            </p>
          </div>
          <h3
            dangerouslySetInnerHTML={{
              __html: ElektronikaData?.data[0]?.title.rendered
            }}
          ></h3>
          <p
            dangerouslySetInnerHTML={{
              __html: ElektronikaData?.data[0]?.content.rendered
                .slice(0, 200)
                .concat("...")
            }}
          ></p>
        </div>
      </Link>
      <Link to={`/news/${ElektronikaData?.data[1]?.id}`}>
        <div className="newElektrotexnikaCard">
          <div className="newElektrotexnikaCardText">
            <h4
              dangerouslySetInnerHTML={{
                __html: ElektronikaData?.data[1]?.title.rendered
              }}
            ></h4>
            <p>
              {ElektronikaData?.data[1]?.date
                ?.replace(/-/g, ".")
                .slice(0, 10)
                .split(".")
                .reverse()
                .join(".")}
            </p>
          </div>
          <div className="newElektrotexnikaCardImg">
            <div>
              <img
                src={ElektronikaData?.data[1]?._embedded[
                  "wp:featuredmedia"
                ]?.map((img) => {
                  return img.source_url;
                })}
                alt=""
              />
            </div>
          </div>
        </div>
      </Link>
      <Link to={`/news/${ElektronikaData?.data[2]?.id}`}>
        <div className="newElektrotexnikaCard">
          <div className="newElektrotexnikaCardText">
            <h4
              dangerouslySetInnerHTML={{
                __html: ElektronikaData?.data[2]?.title.rendered
              }}
            ></h4>
            <p>
              {ElektronikaData?.data[2]?.date
                ?.replace(/-/g, ".")
                .slice(0, 10)
                .split(".")
                .reverse()
                .join(".")}
            </p>
          </div>
          <div className="newElektrotexnikaCardImg">
            <div>
              <img
                src={ElektronikaData?.data[2]?._embedded[
                  "wp:featuredmedia"
                ]?.map((img) => {
                  return img.source_url;
                })}
                alt=""
              />
            </div>
          </div>
        </div>
      </Link>
      <Link to={`/news/${ElektronikaData?.data[3]?.id}`}>
        <div className="newElektrotexnikaCard">
          <div className="newElektrotexnikaCardText">
            <h4
              dangerouslySetInnerHTML={{
                __html: ElektronikaData?.data[3]?.title.rendered
              }}
            ></h4>
            <p>
              {ElektronikaData?.data[3]?.date
                ?.replace(/-/g, ".")
                .slice(0, 10)
                .split(".")
                .reverse()
                .join(".")}
            </p>
          </div>
          <div className="newElektrotexnikaCardImg">
            <div>
              <img
                src={ElektronikaData?.data[3]?._embedded[
                  "wp:featuredmedia"
                ]?.map((img) => {
                  return img.source_url;
                })}
                alt=""
              />
            </div>
          </div>
        </div>
      </Link>
    </section>
  );
}

export default NewElektronika;
