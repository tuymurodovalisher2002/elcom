export const queryName = {
  NEWS: "NEWS",
  // ANSWER: "ANSWER",
  ANSWERHOME: "ANSWERHOME",
  // IMG: "IMG",
  FOOTER: "FOOTER",
  // FOOTERIMG: "FOOTERIMG",
  // NEWARTICLE: "NEWARTICLE",
  // NEWARTICLEACTIVE: "NEWARTICLEACTIVE",
  // ELECTRICALARTICLENEWS: "ELECTRICALARTICLENEWS",
  // ELECTRICALARTICLENEWSACTIVE: "ELECTRICALARTICLENEWSACTIVE",
  ANSWERQUESTION: "ANSWERQUESTION",
  ELEKTROTEXNIKA: "ELEKTROTEXNIKA",
  ELEKTRONIKA: "ELEKTRONIKA",
  AVTOMATIKA: "AVTOMATIKA",
  // BOOKS: "BOOKS",
  // ALLARTICLE: "ALLARTICLE",
  POST_ID:"POST_ID",
  ELEKTRONIKABOOKS: "ELEKTRONIKABOOKS",
  ELEKTRONIKABOOKSMEDIA: "ELEKTRONIKABOOKSMEDIA",
  ELEKTROTEXNIKABOOKS: "ELEKTROTEXNIKABOOKS",
  ELEKTROTEXNIKABOOKSMEDIA: "ELEKTROTEXNIKABOOKSMEDIA",
  AVTOMATIKABOOKS: "AVTOMATIKABOOKS",
  AVTOMATIKABOOKSMEDIA: "AVTOMATIKABOOKSMEDIA",
  SEARCH: "SEARCH",
  MEDIA: "MEDIA",
};
