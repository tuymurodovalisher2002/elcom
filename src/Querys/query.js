import { useQuery } from "react-query";
import $api from "../Service/requestService";
import { queryName } from "./queryName";

export function useNews() {
  return useQuery([queryName.NEWS], async () => {
    const res = await $api.get("/posts?categories=5&_embed");
    return res.data;
  });
}

// export function useAnswer() {
//   return useQuery([queryName.ANSWER], async () => {
//     const res = await $api.get("/posts?categories=7");
//     return res.data;
//   });
// }

export function useFooter() {
  return useQuery([queryName.FOOTER], async () => {
    const res = await $api.get("/posts?categories=9&_embed");
    return res.data;
  });
}

// export function useNewArticle() {
//   return useQuery([queryName.NEWARTICLE], async () => {
//     const res = await $api.get("/posts?categories=10&_embed");
//     return res.data;
//   });
// }

// export function useNewArticleActice() {
//   return useQuery([queryName.NEWARTICLEACTIVE], async () => {
//     const res = await $api.get("/posts?categories=11&_embed");
//     return res.data;
//   });
// }

// export function useElectricalArticleNews() {
//   return useQuery([queryName.ELECTRICALARTICLENEWS], async () => {
//     const res = await $api.get("/posts?categories=12&_embed");
//     return res.data;
//   });
// }

// export function useElectricalArticleNewsActive() {
//   return useQuery([queryName.ELECTRICALARTICLENEWSACTIVE], async () => {
//     const res = await $api.get("/posts?categories=13&_embed");
//     return res.data;
//   });
// }

export function useAnswerQuestion(page) {
  return useQuery([queryName.ANSWERQUESTION, page], async () => {
    const res = await $api.get("/posts?categories=14&_embed&page=" + page);
    return res;
  });
}

export function useAnswer() {
  return useQuery([queryName.ANSWERHOME], async () => {
    const res = await $api.get("/posts?categories=14");
    return res;
  });
}

// export function useElektrotexnika() {
//   return useQuery([queryName.ELEKTROTEXNIKA], async () => {
//     const res = await $api.get("/posts?categories=15&_embed");
//     return res.data;
//   });
// }

// export function useAllArticle() {
//   return useQuery([queryName.ALLARTICLE], async () => {
//     const res = await $api.get("/posts?categories=16&_embed");
//     return res.data;
//   });
// }
export function usePost(id) {
  return useQuery([queryName.POST_ID, id], async () => {
    const res = await $api.get("/posts/" + id + "?_embed");
    return res.data;
  });
}

export function useElektrotexnika(page) {
  return useQuery([queryName.ELEKTROTEXNIKA, page], async () => {
    const res = await $api.get("/posts?categories=17&_embed&page=" + page);
    return res;
  });
}

export function useElektronika(page) {
  return useQuery([queryName.ELEKTRONIKA, page], async () => {
    const res = await $api.get("/posts?categories=18&_embed&page=" + page);
    return res;
  });
}

export function useActomatika(page) {
  return useQuery([queryName.AVTOMATIKA, page], async () => {
    const res = await $api.get("/posts?categories=19&_embed&page=" + page);
    return res;
  });
}

// export function useAdabiyotlar() {
//   return useQuery([queryName.BOOKS], async () => {
//     const res = await $api.get("/posts?categories=20&_embed");
//     return res.data;
//   });
// }

export function useElektrotexnikaBooks(page) {
  return useQuery([queryName.ELEKTROTEXNIKABOOKS, page], async () => {
    const res = await $api.get("/posts?categories=21&page=" + page);
    return res;
  });
}
export function useElektrotexnikaBooksMedia(page) {
  return useQuery([queryName.ELEKTROTEXNIKABOOKSMEDIA, page], async () => {
    const res = await $api.get("/media?categories=21&page=" + page);
    return res;
  });
}

export function useMedia(page) {
  return useQuery([queryName.MEDIA, page], async () => {
    const res = await $api.get("/media?&per_page=100");
    return res;
  });
}

export function useElektronikaBooksMedia(page) {
  return useQuery([queryName.ELEKTRONIKABOOKSMEDIA, page], async () => {
    const res = await $api.get("/media?categories=22&page=" + page);
    return res;
  });
}

export function useElektronikaBooks(page) {
  return useQuery([queryName.ELEKTRONIKABOOKS, page], async () => {
    const res = await $api.get("/posts?categories=22&page=" + page);
    return res;
  });
}

export function useAvtomatikaBooks(page) {
  return useQuery([queryName.AVTOMATIKABOOKS, page], async () => {
    const res = await $api.get("/posts?categories=23&page=" + page);
    return res;
  });
}

export function useAvtomatikaBooksMedia(page) {
  return useQuery([queryName.AVTOMATIKABOOKSMEDIA, page], async () => {
    const res = await $api.get("/media?categories=23&page=" + page);
    return res;
  });
}

export function useSearch(text, page) {
  return useQuery([queryName.SEARCH, text, page], async () => {
    const res = await $api.get("posts?search=" + text + "&page=" + page);
    return res;
  });
}
