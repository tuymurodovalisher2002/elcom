import React from "react";

function AnswerQuestionCard(props) {
  console.log(props);
  const getAnswerBg = () => {
    if (props.name === "<p>Elektrotexnika</p>\n") {
      return "#4dbf50";
    } else if (props.name === "<p>Elektronika</p>\n") {
      return "#fabd00";
    } else if (props.name === "<p>Avtomatika</p>\n") {
      return "#6562ed";
    } else {
      return "#e63233";
    }
  };

  return (
    <div className="answerQuestionCard" pagination={true}>
      <div>
        <h6
          dangerouslySetInnerHTML={{ __html: props.name }}
          style={{ background: getAnswerBg() }}
        ></h6>
        <p>
          {/* <span>
            <img src={props.img} alt="" />
            <div className="post-views post-234 entry-meta">
              <span class="post-views-count">1</span>
            </div>
          </span> */}
          <span>{props.date}</span>
          <span
            dangerouslySetInnerHTML={{
              __html: props.author
            }}
          ></span>
        </p>
      </div>
      <h4 dangerouslySetInnerHTML={{ __html: props.title }}></h4>
      <p
        dangerouslySetInnerHTML={{
          __html: props.text
        }}
      ></p>
    </div>
  );
}

export default AnswerQuestionCard;
