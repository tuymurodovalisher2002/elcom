import { Col, Row, Spin } from "antd";
import React from "react";
import { useParams } from "react-router-dom";
import { usePost } from "../Querys/query";
// import quotes from "../Articles/quotes.png";
// import "./Article.scss";
import NewsArticle from "../Home/NewsArticle";
// import telegram from "../News/telegram.png";
// import youtube from "../News/youtube.png";
import "./Responsive.scss";
import {
  FacebookIcon,
  FacebookShareButton,
  TelegramIcon,
  TelegramShareButton,
  TwitterIcon,
  TwitterShareButton
} from "react-share";
import { Helmet } from "react-helmet-async";

function AnswerPost() {
  const location = useParams();
  const { data, isLoading } = usePost(location.id);
  // console.log(data);

  return (
    <>
      {/* <meta name="description" content={data?.title.rendered} />
      <meta name="keywords" content={data?.content.rendered} />
      <meta
        property="og:title"
        content={data?.title.rendered.slice(0, 50).concat("...")}
      />
      <meta
        property="og:description"
        content={data?.content.rendered.slice(5, 150).concat("...")}
      />
      <meta property="og:type" content="website" />
      <meta property="og:url" content="https://elcominfo.uz/" />
      <meta property="og:site_name" content="elcominfo.uz" /> */}
      <Helmet>
        <title>Elcom. Savol-javoblar</title>
        <meta
          name="description"
          content={data?.content.rendered
            .replace(/<\/?[^>]+>/gi, "")
            .replace(/&#8217;/gi, "'")
            .slice(0, 96)
            .concat("...")}
        />
        <link rel="canonical" href={"/answerPosts/" + data?.id} />
        <meta name="robots" content="index,follow" />
        <meta name="keywords" content={data?.title.rendered} />
        <meta
          property="og:title"
          content={data?.title.rendered
            .replace(/<\/?[^>]+>/gi, "")
            .replace(/&#8217;/gi, "'")
            .slice(0, 50)}
        />
        <meta
          property="og:description"
          content={data?.content.rendered
            .replace(/<\/?[^>]+>/gi, "")
            .replace(/&#8217;/gi, "'")
            .slice(0, 110)
            .concat("...")}
        />
        <meta property="og:type" content="website" />
        <meta
          property="og:url"
          content={`https://elcominfo.uz/answerPosts/` + data?.id}
        />
        <meta property="og:site_name" content="elcominfo.uz" />
        <meta
          property="og:image"
          content={
            data?._embedded["wp:featuredmedia"]
              ? data?._embedded["wp:featuredmedia"]?.map((img) => {
                  return img.source_url;
                })
              : "https://admin.elcominfo.uz/wp-content/uploads/2022/04/facicon-1.png"
          }
        />
        <meta property="og:image:witdh" content="1200" />
        <meta property="og:image:height" content="600" />
        <meta property="og:image:alt" content="Elcom Savollar" />
        <meta property="og:locale" content="uz_uz" />

        {/* Twitter Card SEO  */}

        <meta
          name="twitter:title"
          content={data?.title.rendered
            .replace(/<\/?[^>]+>/gi, "")
            .replace(/&#8217;/gi, "'")
            .slice(0, 50)}
        />
        <meta
          name="twitter:description"
          content={data?.content.rendered
            .replace(/<\/?[^>]+>/gi, "")
            .replace(/&#8217;/gi, "'")
            .slice(0, 110)
            .concat("...")}
        />
        <meta
          name="twitter:image"
          content={
            data?._embedded["wp:featuredmedia"]
              ? data?._embedded["wp:featuredmedia"]?.map((img) => {
                  return img.source_url;
                })
              : "https://admin.elcominfo.uz/wp-content/uploads/2022/04/facicon-1.png"
          }
        />
        <meta name="twitter:card" content="summary_large_image" />
      </Helmet>
      <section className="posts">
        <div className="container-fluid">
          <Row>
            <Col md={{ span: 13 }}>
              <Spin spinning={isLoading}>
                <div className="postInfo">
                  <div style={{ margin: "0 0 15px 0" }}>
                    <h5
                      dangerouslySetInnerHTML={{
                        __html:
                          data?.excerpt.rendered === ""
                            ? data?.excerpt.rendered + "Adabiyotlar"
                            : data?.excerpt.rendered
                      }}
                    ></h5>
                    <p>
                      {data?.date
                        ?.replace(/-/g, ".")
                        .slice(0, 10)
                        .split(".")
                        .reverse()
                        .join(".")}
                    </p>
                  </div>
                  <h2
                    dangerouslySetInnerHTML={{ __html: data?.title.rendered }}
                  ></h2>
                  <img
                    alt=""
                    src={data?._embedded["wp:featuredmedia"]?.map((img) => {
                      return img.source_url;
                    })}
                    style={{ margin: "20px 0" }}
                  />
                  {/* <div className="quotes">
                  <img alt="asdas" src={quotes} />
                </div> */}
                  <p
                    // dangerouslySetInnerHTML={{ __html: data?.content.rendered }}
                    className="articleText"
                    style={{
                      padding: "20px 0 0 0",
                      marginTop: "20px",
                      borderTop: "1px solid #cfcfcf"
                    }}
                  >
                    {data?.content.rendered
                      .replace(/<\/?[^>]+>/gi, "")
                      .replace(/&#8217;/gi, "'")}
                  </p>
                </div>
                <div className="newsSocialNetwork">
                  <p>Elcom</p>
                  <div>
                    Ulashish
                    {/* <a href="https://t.me/ELCOM_ELEKTRONIKA">
                    <img src={telegram} alt="" />
                  </a>
                  <a href="https://www.youtube.com/channel/UCb1F5CyKZ2R_ppfC8zwvHsQ">
                    <img src={youtube} alt="" />
                  </a> */}
                    <FacebookShareButton
                      style={{ marginLeft: "11px" }}
                      url={"http://elcominfo.uz/" + `answerPosts/${data?.id}`}
                      quote="Elcominfo. Elektronika, Avtomatika va Elektrotexnikaga doir barcha kerakli manbalar to'plami"
                    >
                      <FacebookIcon size={24} round={true} />
                    </FacebookShareButton>
                    <TelegramShareButton
                      style={{ marginLeft: "11px" }}
                      url={"http://elcominfo.uz/" + `answerPosts/${data?.id}`}
                      quote="Elcominfo. Elektronika, Avtomatika va Elektrotexnikaga doir barcha kerakli manbalar to'plami"
                    >
                      <TelegramIcon size={24} round={true} />
                    </TelegramShareButton>
                    <TwitterShareButton
                      style={{ marginLeft: "11px" }}
                      url={"http://elcominfo.uz/" + `answerPosts/${data?.id}`}
                      quote="Elcominfo. Elektronika, Avtomatika va Elektrotexnikaga doir barcha kerakli manbalar to'plami"
                    >
                      <TwitterIcon size={24} round={true} />
                    </TwitterShareButton>
                  </div>
                </div>
              </Spin>
            </Col>
            <Col md={{ span: 11 }}>
              <NewsArticle />
            </Col>
          </Row>
        </div>
      </section>
    </>
  );
}

export default AnswerPost;
