import { Button, Col, Pagination, Row, Spin } from "antd";
import React, { useState } from "react";
import { useAnswerQuestion } from "../Querys/query";
import AnswerQuestionCard from "./AnswerQuestionCard";
import "./AnswerQuestion.scss";
// import NewAnswerArticle from "./NewAnswerArticle";
import NewsArticle from "../Home/NewsArticle";
import { Link } from "react-router-dom";
import "./Responsive.scss";
import { Helmet } from "react-helmet";

function AnswerQuestion() {
  const [totslPage, setTotalPage] = useState(1);
  const { data: AnswerQuestionData, isLoading: AnswerQuestionLoading } =
    useAnswerQuestion(totslPage);

  console.log(typeof AnswerQuestionData?.headers["x-wp-total"]);
  const clickLink = () => {
    window.scrollTo({
      top: 80
    });
  };
  return (
    <>
      <Helmet>
        <title>Elcom. Savol-javoblar</title>
        <meta
          name="description"
          data-rh="true"
          content="Savollar, Elektronika, Avtomatika, Elektrotexnikaga doir barcha savollarga to'liq javoblar to'plami"
        />
        <link rel="canonical" href="/answer" />
        <meta name="robots" content="index,follow" />
        <meta
          name="keywords"
          content="Elcom Savollar, Elektronika Savollar, Elektrotexnika Savollar, Avtomatika Savollar"
        />
        <meta
          property="og:title"
          content="Elcom Savollar, Elektronika Savollar, Elektrotexnika Savollar, Avtomatika Savollar"
        />
        <meta
          property="og:description"
          content="Savollar, Elektronika, Avtomatika, Elektrotexnikaga doir barcha savollarga to'liq javoblar to'plami"
        />
        <meta property="og:type" content="website" />
        <meta property="og:url" content={`https://elcominfo.uz/answer`} />
        <meta property="og:site_name" content="elcominfo.uz" />
        <meta
          property="og:image"
          content="https://admin.elcominfo.uz/wp-content/uploads/2022/04/facicon-1.png"
        />
        <meta
          type="image/png"
          name="link"
          href="https://admin.elcominfo.uz/wp-content/uploads/2022/04/facicon-1.png"
        />
      </Helmet>
      <section className="answerQuestion">
        <div className="container-fluid">
          <Row>
            <Col
              md={{ span: 14 }}
              pagination={true}
              style={{ paddingRight: "60px" }}
            >
              <div className="answerTitle">
                <h3 className="title-large">Savol-javoblar</h3>
                <a href="https://t.me/+eMXp2ylO2ugzZTU6">
                  <Button>Savol berish</Button>
                </a>
              </div>
              <Spin spinning={AnswerQuestionLoading}>
                {AnswerQuestionData?.data?.map((data) => {
                  return (
                    <Link
                      to={`/answerPosts/${data.id}`}
                      style={{ width: "100%" }}
                      onClick={clickLink}
                    >
                      <AnswerQuestionCard
                        name={data?.excerpt.rendered}
                        // view={data?.id}
                        img={data?._embedded["wp:featuredmedia"]?.map((img) => {
                          return img.source_url;
                        })}
                        date={data?.date
                          ?.replace(/-/g, ".")
                          .slice(0, 10)
                          .split(".")
                          .reverse()
                          .join(".")}
                        // author={data?._embedded["wp:featuredmedia"]?.map(
                        //   (author) => {
                        //     return author.caption.rendered;
                        //   }
                        // )}
                        title={data?.title.rendered.slice(0, 100).concat("...")}
                        text={data?.content.rendered
                          .trim()
                          .slice(0, 300)
                          .concat("...")}
                      />
                    </Link>
                  );
                })}
              </Spin>
              <div className="paginations">
                {AnswerQuestionData?.headers["x-wp-total"] > 10 ? (
                  <Pagination
                    total={Number(AnswerQuestionData?.headers["x-wp-total"])}
                    onChange={(page) => setTotalPage(page)}
                  />
                ) : null}
                {/* <Pagination
                total={Number(AnswerQuestionData?.headers["x-wp-total"])}
                onChange={(page) => setTotalPage(page)}
              /> */}
              </div>
            </Col>
            <Col md={{ span: 10 }}>
              <NewsArticle />
            </Col>
          </Row>
        </div>
      </section>
    </>
  );
}

export default AnswerQuestion;
